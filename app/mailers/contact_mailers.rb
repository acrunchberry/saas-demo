class ContactMailer < ActionMailer::Base
    
    default to: 'support@mauradermedia.com'
   
    def contact_email(name, email, body)
           @name = name
           @email = email
           @body = body
           
           mail(from: email, subject: 'Contact form Mesasge')
    end
    
end